# Clears the screen when the code is executed
system "cls"

# Declaring variables at differents allowed ways
# Remember that names with upercase is restrict to class names.
#   VeiculoClass - Class name
#   veiculoClass - variable name
first_name = "André Felipe" # The most commom way for ruby programmers (conventions).
lastName = "Camargo Leite"
age = 25

puts first_name
puts age
