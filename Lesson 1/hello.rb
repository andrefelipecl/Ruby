# puts function add a new line at the end of the printed message
puts "Hello World with 'puts' function."

# print functions do not add a new line at the end of the line.
print "Hello World with 'print' function."