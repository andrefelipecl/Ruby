require "gosu"

$width_dim = 800
$height_dim = 600

module ZOrder
    BACKGROUND, STARS, PLAYER, UI = *0..3
end

class Tutorial < Gosu::Window
    def initialize
        super $width_dim, $height_dim
        self.caption = "My Awesome Space Game"
        @background_image = Gosu::Image.new("media/bg/space2.png", :tileable => true)
        
        @player = Player.new
        @player.warp($width_dim/2, $height_dim/2)

        @star_anim = Gosu::Image.load_tiles("media/obj/star.png", 25, 25)
        @stars = Array.new
    end

    # this method is called by Gosu to update the things on screen
    # about sixty times per second.
    # Generally we put the main game logic of the game inside the method, like:
    #   - Moving, pumping, coliding and etc.
    def update 
        if Gosu.button_down? Gosu::KB_LEFT or Gosu::button_down? Gosu::GP_LEFT
            @player.turn_left
        end
        if Gosu.button_down? Gosu::KB_RIGHT or Gosu::button_down? Gosu::GP_RIGHT
            @player.turn_right
        end
        if Gosu.button_down? Gosu::KB_UP or Gosu::button_down? Gosu::GP_BUTTON_0
            @player.accelerate
        end
        @player.move
        @player.collect_stars(@stars)

        if rand(100) < 4 and @stars.size < 25
            @stars.push(Star.new(@star_anim))
        end
    end

    # This method has the behavior to draw things on screen.
    # No one logic belongs here, only draw stuffs.
    def draw
        @background_image.draw(0, 0, ZOrder::BACKGROUND)
        @player.draw
        @stars.each { |star| star.draw }
    end

    def button_down(id)
        if id == Gosu::KB_ESCAPE
            close
        else
            super
        end
    end
end

# Our main player class
class Player
    def initialize
        @image = Gosu::Image.new("media/obj/fighter02.png")
        @x = @y = @vel_x = @vel_y = @angle = 0.0
        @score = 0
    end
    
    def warp(x, y)
        @x, @y = x, y
    end

    def turn_left
        @angle -= 5.0
    end

    def turn_right
        @angle += 5.0
    end

    def accelerate
        @vel_x += Gosu.offset_x(@angle, 0.5)
        @vel_y += Gosu.offset_y(@angle, 0.5)
    end

    def move
        @x += @vel_x
        @y += @vel_y
        @x %= $width_dim
        @y %= $height_dim

        @vel_x *= 0.95
        @vel_y *= 0.95
    end

    def draw
        @image.draw_rot(@x, @y, 1, @angle)
    end

    def score
        @score
    end
    
    def collect_stars(stars)
        stars.reject! { |star| Gosu.distance(@x, @y, star.x, star.y) < 35 }
    end
end


# Add some big star animations
class Star

    attr_reader :x, :y

    def initialize(animation)
        @animation = animation
        @color = Gosu::Color::BLACK.dup
        @color.red = rand(256-40) + 40
        @color.green = rand(256-4) + 40
        @color.blue = rand(256-4) + 40
        @x = rand * $width_dim
        @y = rand * $height_dim
    end 

    def draw
        img = @animation[Gosu.milliseconds  / 100 % @animation.size]
        img.draw(@x - img.width / 2.0, @y - img.height / 2.0, 
        ZOrder::STARS, 1, 1, @color, :add)
    end
end


Tutorial.new.show
