=begin
    Using Methods with Ruby. 
=end
system "cls"

# Methods
def say_hello(first_name, last_name)
    return "Hello, #{first_name} #{last_name}!"
end

print "Enter your First Name: "
first_name = gets.chomp

print "Enter your Last Name: "
last_name = gets.chomp

REGEX_CRYPT = "xdaxs"
puts say_hello(first_name, last_name)
crypt_name = first_name.crypt(REGEX_CRYPT) # one way crypt
puts "Your crypt name is: %s" % [crypt_name]