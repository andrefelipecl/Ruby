=begin
    """Fizz Buzz"""

    Imagine printing out on the screen numbers from one to a hundred.
    So, imagine if the number is divisble by 3, we'll print 'Fizz';
    And if the number is divisible by 5, we'll prnt 'Buzz'; And if the
    number is divisible by 3 and 5, we'll print 'Fizz Buzz'; Otherwise,
    just print the number.
=end

messages = ["Fizz", "Buzz", "Fizz Buzz"]

(1..100).each do |n|
    if n % 3 == 0 && n % 5 == 0
        puts "#{n} - Fizz Buzz"
    elsif n % 3 == 0
        puts "#{n} - Fizz"
    elsif n % 5 == 0
        puts "#{n} - Buzz"
    else
        puts "#{n}."
    end
end