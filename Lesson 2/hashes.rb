=begin
    Using Hashes with Ruby.

    We could think in a Hash like a fancy array. Its helps us when we
    have got a complex list. 
=end

#Simple List
names = ["Andre", "Felipe", "Camargo", "Leite"]
puts names

# Hash List - We work with a key and its value.
favorite_pizza = {
    names[0] => "Pepperoni",
    names[1] => "Mushroom",
    names[2] => "Cheese",
    names[3] => "Chocolate"
}
puts favorite_pizza

# adding a new item
favorite_pizza["Bob"] = "Onion"
puts favorite_pizza["Bob"]

# If you set a already existing key, the value will be overwritten
favorite_pizza["Andre"] = "Kiwi"
puts favorite_pizza["Andre"]

# To delete somenthing
favorite_pizza.delete("Camargo")
puts favorite_pizza