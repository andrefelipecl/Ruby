=begin
    The different kinds of loop.
=end

first_name = "André Felipe"
first_name.each_char do |letter|
    puts letter
end

num = [1,2,3,4,5,6]
num.each do |n|
  puts n
end
