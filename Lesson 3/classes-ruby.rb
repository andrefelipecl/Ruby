=begin
  This code will show us how to use and define a Ruby Class.
  We are using getters and setters methods.
  The initialization method and how to instatiate a class.
=end

system "cls"

# Classes
class Square

  # attr_reader - Shortcut Ruby to create getters for all attributes
  # attr_writer - Shortcut Ruby to create setters for all attributes
  # attr_accessor - Shortcut Ruby to create both getters and setters for all attributes
  
  # You can write here all the params you want separeted by space
  attr_accessor :side_length

  def initialize(side_length)
    @side_length = side_length
  end

  def perimeter
    return @side_length * 4
  end

  def area
    return @side_length**2
  end

  def to_s
    return "Side Length: #{@side_length}\nArea: #{area}\nPerimeter: #{perimeter}"
  end

  def draw
    puts "*" * @side_length
    (@side_length-2).times do 
      print "*" + (" " * (@side_length - 2)) + "*\n"
    end
    puts "*" * @side_length
  end

end

my_square = Square.new(10)
puts "The perimeter of square is #{my_square.perimeter}"
puts "The area of square is #{my_square.area}"
puts my_square
puts my_square.draw